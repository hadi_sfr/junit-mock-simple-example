import org.junit.*;

import java.io.IOException;
import java.util.Vector;

import static org.junit.Assert.*;

public class MeetingManagerTest {
    MeetingManagerUnderTest meetingManagerUnderTest;
    static TestMockServer mockServer = new TestMockServer();

    @BeforeClass
    public static void fixture() {
        mockServer.startServer();
        mockServer.createExpectationForValidDate();
        mockServer.createExpectationForValidEmail();
        mockServer.createExpectationForInvalidEmail();
//        mockServer.createExpectationForInvalidDate();
    }

    @AfterClass
    public static void cleanUp() {
        mockServer.stopServer();
    }

    @Before
    public void setUp() {
        this.meetingManagerUnderTest = MeetingManagerUnderTest.getInstanceUnderTest();
    }

    @Test
    public void MeetingManagerOnInitShouldBeEmptyTest() throws IOException {
        meetingManagerUnderTest.notifyMeetings();
    }

    @Test
    public void MeetingManagerAddTest() throws Exception {
        Vector<Person> persons = new Vector<Person>();
        meetingManagerUnderTest.setDateValidatorUrl("http://127.0.0.1:1080/checkdate");
        Person person1 = new Person("person1", "one@ut.ac.ir", "http://127.0.0.1:1080");
        Person person2 = new Person("person2", "two@ut.ac.ir", "http://127.0.0.1:1080");
        persons.add(person1);
        persons.add(person2);
        meetingManagerUnderTest.bookMeeting(persons, 1);
        Assert.assertEquals(1, meetingManagerUnderTest.db.size());
        Meeting addedMeeting = meetingManagerUnderTest.db.get(0);
        Assert.assertTrue(addedMeeting.getAttendees().contains(person1));
        Assert.assertTrue(addedMeeting.getAttendees().contains(person2));
    }

    @Test
    public void MeetingManagerAddAndNotifySuccessfulTest() throws Exception {
        Vector<Person> persons = new Vector<Person>();
        meetingManagerUnderTest.setDateValidatorUrl("http://127.0.0.1:1080/checkdate");
        persons.add(new Person("valid", "pass@ut.ac.ir", "http://127.0.0.1:1080"));
        meetingManagerUnderTest.bookMeeting(persons, 1);
        meetingManagerUnderTest.notifyMeetings();
    }

    @Test(expected = Exception.class)
    public void MeetingManagerAddAndNotifyUnsuccessfulTest() throws Exception {
        Vector<Person> persons = new Vector<Person>();
        meetingManagerUnderTest.setDateValidatorUrl("http://127.0.0.1:1080/checkdate");
        persons.add(new Person("invalid", "fail@ut.ac.ir", "http://127.0.0.1:1080"));
        meetingManagerUnderTest.bookMeeting(persons, 1);
        meetingManagerUnderTest.notifyMeetings();
    }
}