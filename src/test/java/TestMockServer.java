import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.mockserver.client.server.MockServerClient;
import org.mockserver.integration.ClientAndServer;

import static org.mockserver.integration.ClientAndServer.*;
import static org.mockserver.matchers.Times.*;
import static org.mockserver.model.HttpRequest.*;
import static org.mockserver.model.HttpResponse.*;
import static org.mockserver.model.RegexBody.regex;
import static org.mockserver.model.StringBody.*;

public class TestMockServer {

    private ClientAndServer mockServer;
    private MockServerClient client = new MockServerClient("127.0.0.1", 1080);

    @BeforeClass
    public void startServer() {
        mockServer = startClientAndServer(1080);
        System.out.println("http://localhost:1080 is up.");
    }

    @AfterClass
    public void stopServer() {
        mockServer.stop();
        System.out.println("http://localhost:1080 is down.");
    }

    void createExpectationForValidEmail() {
        new MockServerClient("127.0.0.1", 1080)
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/")
                                .withBody(exact("email=pass@ut.ac.ir&name=valid&type=notify")),
                        unlimited())
                .respond(
                        response()
                                .withStatusCode(200)
                                .withDelay(TimeUnit.SECONDS, 1)
                );
    }

    void createExpectationForInvalidEmail() {
        new MockServerClient("127.0.0.1", 1080)
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/")
                                .withBody(exact("email=fail@ut.ac.ir&name=invalid&type=notify")),
                        unlimited())
                .respond(
                        response()
                                .withStatusCode(200)
                                .withDelay(TimeUnit.SECONDS, 1)
                );
    }

    void createExpectationForValidDate() {
        client
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/checkdate")
//                                .withBody("{date=.*}")
                        ,
                        unlimited())
                .respond(
                        response()
                                .withStatusCode(200)
                                .withDelay(TimeUnit.SECONDS, 1)
                );
    }

    void createExpectationForInvalidDate() {
        new MockServerClient("127.0.0.1", 1080)
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/checkdate")
                                .withBody(regex("{date=*}")),
                        unlimited())
                .respond(
                        response()
                                .withStatusCode(500)
                                .withDelay(TimeUnit.SECONDS, 1)
                );
    }
}