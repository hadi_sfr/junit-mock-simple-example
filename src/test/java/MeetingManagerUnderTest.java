import java.sql.Date;
import java.util.Vector;

public class MeetingManagerUnderTest extends MeetingManager {
    private static MeetingManagerUnderTest ourInstance = new MeetingManagerUnderTest();
    Vector<Meeting> db = new Vector<Meeting>();

    public static MeetingManagerUnderTest getInstanceUnderTest() {
        return ourInstance;
    }

    @Override
    Vector<Meeting> getAllMeetings() {
        return db;
    }

    @Override
    void saveMeeting(Meeting meeting) {
        System.out.println(meeting.getAttendees().get(0).toString());
        System.out.println(String.join(meeting.getAttendees().toString(), ", "));
        System.out.println(new Date(meeting.getDate().getTime()));
        db.add(meeting);
    }
}
